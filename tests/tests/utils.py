from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType

from admin_field_permissions.models import PermissionStructureModel


def generate_group(name):
    group, _ = Group.objects.get_or_create(name=name)
    return group


# Generate different permission structures
def create_permission_structure(group, model):
    model = ContentType.objects.get_for_model(model)
    structure = PermissionStructureModel(group=group, model=model)
    return structure
