import django
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.admin import site
from django.contrib.auth.models import User
from django.test import RequestFactory
from django.test import TestCase
from django.utils.encoding import force_str

from ..models import ExampleModel
from .utils import create_permission_structure
from .utils import generate_group
from admin_field_permissions.mixins import FieldPermissionMixin
from admin_field_permissions.models import PermissionStructureModel
from admin_field_permissions.models import StructField


class TestStructFieldModel(TestCase):
    pass


class TestPermissionStructureModel(TestCase):
    def test_create_permission_structure(self):
        group = generate_group("testing")
        model = ExampleModel
        permission_structure = create_permission_structure(group, model)
        permission_structure.save()
        self.assertEqual(1, PermissionStructureModel.objects.count())
        self.assertEqual(permission_structure, PermissionStructureModel.objects.first())

    def test_create_permission_structure_duplicated(self):
        group = generate_group("testing")
        model = ExampleModel
        permission_structure_original = create_permission_structure(group, model)
        permission_structure_original.save()
        permission_structure_repeated = create_permission_structure(group, model)
        with self.assertRaises(django.db.utils.IntegrityError) as e:
            permission_structure_repeated.save()
        excp = e.exception
        errors = (
            "UNIQUE constraint failed: admin_field_permissions_permissionstructuremodel.group_id, "
            "admin_field_permissions_permissionstructuremodel.model_id",
        )
        self.assertEqual(excp.args, errors)
