"""
Test the admin of the field permission model.
"""
from django.contrib import messages
from django.contrib.admin import site
from django.contrib.admin.views.main import ChangeList
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.test import RequestFactory
from django.test import TestCase
from django.utils.encoding import force_str

from ..models import ExampleModel
from ..models import SimplestModel
from admin_field_permissions.admin import PermissionStructureAdmin
from admin_field_permissions.models import PermissionStructureModel
from admin_field_permissions.models import StructField
from tests.tests.utils import create_permission_structure
from tests.tests.utils import generate_group


class TestPermissionStructureAdmin(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user("foo", "bar@foo.com", "top_secret")
        self.user_admin_data = {
            "username": "admin",
            "email": "admin@mail.com",
            "password": "password_secure",
        }
        self.user_admin = User.objects.create_superuser(**self.user_admin_data)

        self.group_1 = generate_group("group_1")
        self.group_2 = generate_group("group_2")

    def test_validate_fields_method_example_model(self):
        modeladmin = PermissionStructureAdmin(PermissionStructureModel, site)
        request_factory = RequestFactory()
        data = {
            "model": ContentType.objects.get_for_model(ExampleModel).pk,
            "group": self.group_1,
        }
        instance = create_permission_structure(self.group_1, ExampleModel)
        instance.save()
        request = request_factory.post("/", data=data)
        request._messages = messages.storage.default_storage(request)
        modeladmin.validate_fields(request, instance)
        fields = StructField.objects.filter(structure=instance)
        self.assertEqual(5 + 2, fields.count())

    def test_validate_fields_method_simplest_model(self):
        modeladmin = PermissionStructureAdmin(PermissionStructureModel, site)
        request_factory = RequestFactory()
        data = {
            "model": ContentType.objects.get_for_model(SimplestModel).pk,
            "group": self.group_1,
        }
        instance = create_permission_structure(self.group_1, SimplestModel)
        instance.save()
        request = request_factory.post("/", data=data)
        request._messages = messages.storage.default_storage(request)
        modeladmin.validate_fields(request, instance)
        fields = StructField.objects.filter(structure=instance)
        self.assertEqual(1 + 2, fields.count())

    def test_get_list(self):
        instance_1 = create_permission_structure(self.group_1, ExampleModel)
        instance_2 = create_permission_structure(self.group_2, ExampleModel)
        instance_1.save()
        instance_2.save()

        request_factory = RequestFactory()
        modeladmin = PermissionStructureAdmin(PermissionStructureModel, site)

        request = request_factory.get("/")
        request.user = self.user

        changelist = self.get_changelist(request, PermissionStructureModel, modeladmin)

        queryset = changelist.get_queryset(request)
        self.assertEqual(2, queryset.count())

    def get_changelist(self, request, model, modeladmin):
        if getattr(modeladmin, "get_changelist_instance", None):
            return modeladmin.get_changelist_instance(request)

        return ChangeList(
            request,
            model,
            modeladmin.list_display,
            modeladmin.list_display_links,
            modeladmin.list_filter,
            modeladmin.date_hierarchy,
            modeladmin.search_fields,
            modeladmin.list_select_related,
            modeladmin.list_per_page,
            modeladmin.list_max_show_all,
            modeladmin.list_editable,
            modeladmin,
        )
