"""
Test how the admin mixin works in another models.
"""
from datetime import date
from datetime import datetime

from django.contrib import admin
from django.contrib.admin import site
from django.contrib.admin.views.main import ChangeList
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.test import RequestFactory
from django.test import TestCase
from django.utils.encoding import force_str

from ..models import ExampleModel
from .utils import create_permission_structure
from .utils import generate_group
from admin_field_permissions.mixins import FieldPermissionMixin
from admin_field_permissions.models import StructField
from admin_field_permissions.utils import generate_fields_for_structure

# from django.contrib import admin


class FieldPermissionAdmin(FieldPermissionMixin, admin.ModelAdmin):
    model = ExampleModel
    ...


class ExampleModelSimpleAdmin(FieldPermissionMixin, admin.ModelAdmin):
    pass


class ExampleModelExtendedAdmin(FieldPermissionMixin, admin.ModelAdmin):
    fieldsets = [
        (
            "Basics",
            {
                "fields": [
                    "char_field",
                    "json_field",
                    "bool_field",
                    "datetime_field",
                    "date_field",
                ]
            },
        )
    ]


class ExampleModelCalculatedFieldsAdmin(FieldPermissionMixin, admin.ModelAdmin):
    fieldsets = [
        (
            "Basics",
            {
                "fields": [
                    "char_field",
                    "json_field",
                    "bool_field",
                    "datetime_field",
                    "date_field",
                ]
            },
        ),
        ("Calculated", {"fields": ["calculated_field_1"]}),
    ]

    def calculated_field_1(self, instance):
        pass


class TestFieldPermissionMixin(TestCase):
    def setUp(self) -> None:
        self.group_admin = generate_group("admin")
        self.group_others = generate_group("other")
        self.user = User.objects.create_user("foo", "bar@foo.com", "SECRET")

        self.user_admin = User.objects.create_user(
            "admin", "admin@mail.com", "PASSWORD"
        )
        self.user_admin.groups.add(self.group_admin)

        self.instance = ExampleModel.objects.create(
            char_field="test",
            json_field={},
            bool_field=True,
            datetime_field=datetime.now(),
            date_field=date.today(),
        )

    def get_changelist(self, request, model, modeladmin):
        if getattr(modeladmin, "get_changelist_instance", None):
            return modeladmin.get_changelist_instance(request)

        return ChangeList(
            request,
            model,
            modeladmin.list_display,
            modeladmin.list_display_links,
            modeladmin.list_filter,
            modeladmin.date_hierarchy,
            modeladmin.search_fields,
            modeladmin.list_select_related,
            modeladmin.list_per_page,
            modeladmin.list_max_show_all,
            modeladmin.list_editable,
            modeladmin,
        )

    def test_get_fieldsets_with_user_in_not_group(self):
        # Create permission structure
        group = generate_group("admin")
        structure = create_permission_structure(group, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model).pk
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)

        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = [
            "char_field",
            "json_field",
            "bool_field",
            "datetime_field",
            "date_field",
        ]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_non_read_fields(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)

        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = []
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_just_char_field_readable(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        field = fields.get(field="char_field")
        field.read = True
        field.save()
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = ["char_field"]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_just_bool_field_readable(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        field = fields.get(field="bool_field")
        field.read = True
        field.save()
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = ["bool_field"]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_just_json_field_readable(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        field = fields.get(field="json_field")
        field.read = True
        field.save()
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = ["json_field"]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_just_datetime_field_readable(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        field = fields.get(field="datetime_field")
        field.read = True
        field.save()
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = ["datetime_field"]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_just_date_field_readable(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        field = fields.get(field="date_field")
        field.read = True
        field.save()
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = ["date_field"]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)

    def test_get_fieldsets_with_user_in_admin_group_all_fields_readable(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        fields.update(read=True)
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = [
            "char_field",
            "bool_field",
            "datetime_field",
            "pk",
            "json_field",
            "date_field",
            "id",
        ]
        self.assertEqual(set(fieldsets[0][1]["fields"]), set(expected_fields))

    def test_get_fieldsets_with_user_in_admin_group_with_admin_using_fieldsets_calculated(
        self,
    ):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        fields.update(read=True)
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = ExampleModelExtendedAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fieldset = [
            (
                "Basics",
                {
                    "fields": [
                        "char_field",
                        "json_field",
                        "bool_field",
                        "datetime_field",
                        "date_field",
                    ]
                },
            ),
            ["Otros", {"fields": ["id", "pk"]}],
        ]
        self.assertEqual(fieldsets, expected_fieldset)

    def test_get_fieldsets_with_user_in_admin_group_with_admin_using_fieldsets(self):
        # Create permission structure
        structure = create_permission_structure(self.group_admin, ExampleModel)
        structure.save()
        model_id = ContentType.objects.get_for_model(structure.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure)
        # Modify fields according to each tests
        fields = StructField.objects.filter(structure=structure)
        fields.update(read=True)
        # Make request to model admin with mixin implemented with an specific user.

        modeladmin = ExampleModelCalculatedFieldsAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fieldset = [
            (
                "Basics",
                {
                    "fields": [
                        "char_field",
                        "json_field",
                        "bool_field",
                        "datetime_field",
                        "date_field",
                    ]
                },
            ),
            ("Calculated", {"fields": ["calculated_field_1"]}),
            ["Otros", {"fields": ["id", "pk"]}],
        ]
        self.assertEqual(fieldsets, expected_fieldset)

    def test_get_fieldsets_with_user_in_two_groups(self):
        self.user_admin.groups.add(self.group_others)

        structure_1 = create_permission_structure(self.group_admin, ExampleModel)
        structure_2 = create_permission_structure(self.group_others, ExampleModel)
        structure_1.save()
        structure_2.save()
        model_id = ContentType.objects.get_for_model(structure_1.model.model_class()).pk

        # Generate fields for permission structure
        generate_fields_for_structure(model_id, structure_1)
        generate_fields_for_structure(model_id, structure_2)
        # Modify fields according to each tests
        fields_s1 = StructField.objects.filter(structure=structure_1)
        fields_s2 = StructField.objects.filter(structure=structure_2)
        field_s1 = fields_s1.get(field="bool_field")
        field_s2 = fields_s2.get(field="json_field")
        field_s1.read = True
        field_s2.read = True
        field_s1.save()
        field_s2.save()

        modeladmin = FieldPermissionAdmin(ExampleModel, site)
        request_factory = RequestFactory()
        request = request_factory.get("/")
        request.user = self.user_admin

        # Evaluate that the fields appear
        fieldsets = modeladmin.get_fieldsets(request, self.instance)
        expected_fields = ["bool_field", "json_field"]
        self.assertEqual(fieldsets[0][1]["fields"], expected_fields)
