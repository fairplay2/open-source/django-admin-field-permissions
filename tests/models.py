from django.db import models


class SimplestModel(models.Model):
    name = models.CharField(max_length=100)


class ExampleModel(models.Model):
    char_field = models.CharField(max_length=50)
    json_field = models.JSONField()
    bool_field = models.BooleanField()
    datetime_field = models.DateTimeField()
    date_field = models.DateField()


class ForeignModel(models.Model):
    name = models.CharField(max_length=10)


class ExampleRelationshipModel(models.Model):
    char_field = models.CharField(max_length=50)
    json_field = models.JSONField()
    bool_field = models.BooleanField()
    datetime_field = models.DateTimeField()
    date_field = models.DateField()
    other_model_field = models.ForeignKey(ForeignModel, on_delete=models.CASCADE)
