=================================
Django admin field permissions
=================================
Library developed to solve the lack of granularity in Django group permissions.


The main functionality provided by the library is allowing the creation of a structure of permissions
that allows having model admin pages that can change dynamically depending on the group.

Installation
------------
1. Add "admin_field_permissions" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'admin_field_permissions',
    ]

2. Run ``python manage.py migrate`` to create the admin_field_permissions models.
3. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a field permission structure (you'll need the Admin app enabled).


How to use it?
--------------
1. Use the ``FieldPermissionMixin`` in the admin class of the model that
you want::
    from admin_field_permissions.mixins import FieldPermissionMixin
    from django.contrib import admin

    class ClientsModelAdmin(FieldPermissionMixin, admin.ModelAdmin):
    ...


**NOTE**: Consider that ``FieldPermissionMixin`` overwrites ``get_readonly_fields``, ``get_form`` and ``get_fieldset`` methods, so this methods
cannot be changed, otherwise, the library might not work as intended.

2. Create an instance of the Field Permission Structure in the admin with the field permissions that you want
to grant to some group over a specific model.
