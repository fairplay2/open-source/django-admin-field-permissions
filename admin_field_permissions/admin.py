from django.contrib import admin
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.urls.resolvers import URLPattern
from django.utils.html import format_html
from django.utils.translation import gettext as _

from .models import PermissionStructureModel
from .models import StructField
from .utils import generate_fields_for_structure
from .utils import validate_field_permissions_is_valid


class FieldInline(admin.TabularInline):
    model = StructField
    fields = ("field", "read", "change")
    extra = 0
    can_delete = False
    readonly_fields = ["field", "read", "change"]

    def get_readonly_fields(self, request, obj):
        readonly_fields = super().get_readonly_fields(request, obj)

        # Get all the permissions for the user
        if obj:
            permissions = validate_field_permissions_is_valid(obj.group, obj)
            readonly_fields = set(readonly_fields).difference(permissions)
        return readonly_fields


class PermissionStructureAdmin(admin.ModelAdmin):
    change_form_template = "admin/admin_field_templates.html"
    inlines = [FieldInline]
    list_filter = ["model", "group"]
    list_display = ["__str__", "model", "group"]

    def response_add(self, request, obj):
        if "_load_fields" in request.POST:
            return self.validate_fields(request, obj)

    def validate_fields(self, request, obj):
        """
        Hook for validate which fields can be add to PermissionStructureModel.
        """
        qs_model_id = request.POST.get("model", None)
        opts = obj._meta

        # FUNCTION
        generate_fields_for_structure(qs_model_id)

        msg_dict = {"name": opts.verbose_name}

        msg = _('Se cargaron correctamente los campos hacia el modelo "{name}".')
        self.message_user(request, format_html(msg, **msg_dict), messages.SUCCESS)

        redirect_url = (
            f"/admin/{opts.app_label}/{opts.model_name}"
            f"/{obj.id}/change#struct-fields-tab"
        )

        return HttpResponseRedirect(redirect_url)
    
    # Elimina la barra final de las URLs en Django Admin
    def remove_trailing_slash(self, urlpatterns):
        for urlpattern in urlpatterns:
            if isinstance(urlpattern, URLPattern):
                if urlpattern.pattern._route.endswith("/"):
                    urlpattern.pattern._route = urlpattern.pattern._route[:-1]
                elif urlpattern.pattern._route == "":
                    urlpattern.pattern._route = "list"
        return urlpatterns

    def get_urls(self):
        urls = super().get_urls()
        return self.remove_trailing_slash(urls)

admin.site.register(PermissionStructureModel, PermissionStructureAdmin)
