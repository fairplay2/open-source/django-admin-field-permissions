import inspect
from copy import deepcopy
from typing import Tuple

from django.contrib.admin.utils import flatten_fieldsets
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from .models import PermissionStructureModel
from .models import StructField


# TODO: The function will be used each time that a
# new permission is attempted to be added to the database.
def validate_existence_of_model_fields(model, fields):
    """
    Validates that the given model has the given fields.
    """
    # TODO: Find the best way to validate that the model has the given fields.
    for field in fields:
        if not hasattr(model, field):
            raise Exception("Model %s has no field %s" % (model, field))


# TODO: This function will be used in a django cli command
# to test that all the rows in the current database are valid.
def validate_data_from_database():
    """
    Validate that all the data from the table FieldPermission
    is valid.
    """
    ...


def clean_fieldset_fields(fieldsets: Tuple, valid_fields: list):
    """
    Modify in-mem fieldsets fields to contain only the fields from
    the valid_fields list.
    """

    # Fields that only exist in valid_fields
    new_fieldsets = deepcopy(fieldsets)

    for name, opts in new_fieldsets:
        fields = list(opts["fields"])

        final_fields = []
        for field in fields:
            if isinstance(field, (list, tuple)):
                f = tuple([element for element in field if element in valid_fields])
                final_fields.append(f)
            elif field in valid_fields:
                final_fields.append(field)

        opts["fields"] = final_fields

    new_fieldsets.append(
        [
            "Otros",
            {
                "fields": sorted(
                    list(set(valid_fields).difference(flatten_fieldsets(fieldsets)))
                )
            },
        ]
    )

    return new_fieldsets


def validate_field_permissions_is_valid(group, structure):
    """
    Validate that the field permissions are actually valid
    considering the group permisssions.
    """
    if not structure:
        return []
    field_model_contenttype, field_model = (
        ContentType.objects.get_for_model(structure.model.model_class()),
        structure.model,
    )
    user_permissions = Permission.objects.filter(group=group)
    # Validate that the group has actually some type of permissions over
    # the field model

    related_permissions = user_permissions.filter(
        content_type__in=[field_model, field_model_contenttype]
    )

    permissions_limit = []
    if not related_permissions:
        return permissions_limit
    for permission in related_permissions:
        if permission.codename.startswith("change"):
            permissions_limit.append("change")
        if permission.codename.startswith("view"):
            permissions_limit.append("read")

    return list(set(permissions_limit))


def generate_fields_for_structure(model_id, structure=None):

    qs_model = ContentType.objects.get(pk=model_id)
    model_class = qs_model.model_class()

    # Mapping all related fields from the model
    relationed_fields = list(
        related_field for related_field in model_class._meta.fields_map.keys()
    )

    # Load related fields from the model
    field_names = list(i.name for i in model_class._meta.get_fields())

    # Extract only native fields from a model class without relational fields
    filter_fields = list(set(field_names).difference(relationed_fields))

    # Append properties fields
    property_fields = [
        name
        for (name, type) in inspect.getmembers(model_class)
        if isinstance(type, property)
    ]

    structure = (
        PermissionStructureModel.objects.get(model_id=model_id)
        if not structure
        else structure
    )
    for field in property_fields + filter_fields:
        field = StructField(field=field, structure=structure)
        field.save()
