from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.db import models


# Create your models here.
class PermissionStructureModel(models.Model):
    """
    Model to store the permissions for field based in the group.
    """

    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    model = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    # TODO: Add field for "status"
    # TODO: Consider add a JSON Field for the fieldset

    def __str__(self) -> str:
        return f" Group > {str(self.group.name)} | Model > {(self.model.name)}"

    class Meta:
        unique_together = [["group", "model"]]
        verbose_name = "Field permission structure"
        verbose_name_plural = "Field permission structures"


class StructField(models.Model):
    structure = models.ForeignKey(
        PermissionStructureModel,
        on_delete=models.CASCADE,
        blank=False,
        related_name="fields",
    )
    field = models.CharField(
        verbose_name="Fields asociado al modelo seleccionado", max_length=100
    )
    change = models.BooleanField(default=False)
    read = models.BooleanField(default=False)

    class Meta:
        unique_together = [("structure", "field")]

    def __str__(self) -> str:
        return self.field
