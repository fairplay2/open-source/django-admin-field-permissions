from typing import Dict

from django.contrib import admin
from django.contrib.admin.utils import flatten_fieldsets
from django.contrib.contenttypes.models import ContentType

from .models import PermissionStructureModel
from .utils import clean_fieldset_fields


class FieldPermissionMixin(admin.ModelAdmin):
    """
    Mixin class for admin views that allows to define the
    permissions of fields for model in a group.
    """

    all_structure_fields = []

    def _get_model_permissions(self, user):
        """
        Returns a list of permissions for the model in a dictionary
        with the keys for 'read' and 'change' permissions based in
        the group of the user
        """
        contenttype_model, contenttype_proxy = ContentType.objects.get_for_model(
            self.model
        ), ContentType.objects.get_for_model(self.model, for_concrete_model=False)
        field_permissions_structure = PermissionStructureModel.objects.filter(
            model__in=[contenttype_model, contenttype_proxy],
            group__in=user.groups.all(),
        )
        if user.is_superuser:
            return None

        if field_permissions_structure.exists():
            # In case there is more than one permissions structure
            if field_permissions_structure.count() > 1:
                final_permission_dict = {"read": [], "change": [], "all": []}
                # Join the structure permissions
                for group_field in field_permissions_structure:
                    permission_dict = self._group_permission_structure(group_field)
                    final_permission_dict["read"] = sorted(
                        list(
                            set(final_permission_dict["read"]).union(
                                permission_dict["read"]
                            )
                        )
                    )
                    permission_dict["change"] = sorted(
                        list(
                            set(final_permission_dict["change"]).union(
                                permission_dict["change"]
                            )
                        )
                    )
                    final_permission_dict["all"] = sorted(
                        list(
                            set(final_permission_dict["all"]).union(
                                permission_dict["all"]
                            )
                        )
                    )
                return final_permission_dict
            return self._group_permission_structure(field_permissions_structure.first())

        return None

    def _group_permission_structure(self, structure=None) -> Dict:
        return self._group_permissions_from_fk(structure)

    def _group_permissions_from_fk(self, structure) -> dict:
        all_structure_fields = list(
            structure.fields.filter().values_list("field", flat=True)
        )
        self.all_structure_fields.extend(all_structure_fields)
        return {
            "read": (
                list(structure.fields.filter(read=True).values_list("field", flat=True))
            ),
            "change": (
                list(
                    structure.fields.filter(change=True).values_list("field", flat=True)
                )
            ),
            "all": all_structure_fields,
        }

    def get_readonly_fields(self, request, obj=None):
        """
        Returns a list of fields that should be read-only
        for the given request and object.
        """
        permissions_structure = self._get_model_permissions(request.user)

        if permissions_structure:
            # Select fields that does not appear in change permissions
            readonly_fields = set(permissions_structure["read"]).difference(
                permissions_structure["change"]
            )

            return sorted(list(readonly_fields)) + self._get_calculated_fields()
        return self.readonly_fields

    def _get_calculated_fields(self):
        fieldsets = flatten_fieldsets(self.fieldsets) if self.fieldsets else set()
        calculated_fields = list(set(fieldsets).difference(self.all_structure_fields))
        return calculated_fields

    def _get_readable_fields(self, request):
        """
        Return the fields that can be read based in the permissions structure.
        It also considers the fields that are calculated and the fields that can be written if
        they have the option selected.
        """
        permissions_structure = self._get_model_permissions(request.user)
        if permissions_structure:
            return permissions_structure.get("read")
        return None

    def get_form(self, request, obj, change=False, **kwargs):
        allowed_fields = self._get_readable_fields(request)
        if allowed_fields is not None:
            kwargs["fields"] = allowed_fields
        return super().get_form(request, obj, change, **kwargs)

    def get_fieldsets(self, request, obj):
        """
        Hook for specifying fieldsets. It is modified to remove
        the fields that are not returned by `get_fields`
        """
        if self.fieldsets:
            readable_fields = self._get_readable_fields(request)
            if readable_fields is not None:
                valid_fields = list(
                    set(
                        self._get_readable_fields(request)
                        + self._get_calculated_fields()
                    )
                )
                return clean_fieldset_fields(self.fieldsets, valid_fields)
            return self.fieldsets
        return [(None, {"fields": self.get_fields(request, obj)})]
