.PHONY: check-black check-isort check-pylint static-analysis test sdist wheel release pre-release clean

PATH_DADMIN := $(shell which django-admin)
build:
	python setup.py sdist

# Test
.PHONY: test

test:
	@echo "--> Running tests"
	PYTHONWARNINGS=all PYTHONPATH=".:tests:${PYTHONPATH}" django-admin test --settings=tests.settings
